const gulp = require('gulp');
const pug = require('gulp-pug');
const minifyCSS = require('gulp-csso');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const concatCss = require('gulp-concat-css');
const uglify = require('gulp-uglify');
const webpackStream = require('webpack-stream');
const merge = require('merge-stream');
const spritesmith = require('gulp.spritesmith');
const buffer = require('vinyl-buffer');
const imagemin = require('gulp-imagemin');
let plumber = require('gulp-plumber');


gulp.task('pug', () => {
  return gulp.src('src/index.pug')
    .pipe(plumber())
    .pipe(pug())
    .pipe(gulp.dest('dist'));
});

gulp.task('js', () => {
  return gulp.src('src/*.js')
    .pipe(plumber())
    .pipe(webpackStream({
      module: {
        rules: [{
            loader: 'babel-loader',
            query: { presets: ['env'] }
        }]
      },
      externals: {
        jquery: 'jQuery'
      }
    }))
    .pipe(uglify())
    .pipe(concat('app.min.js'))
    .pipe(gulp.dest('dist'));
});

gulp.task('static', () => {
  return gulp.src('src/static/*')
    .pipe(plumber())
    .pipe(gulp.dest('dist/static'));
});

gulp.task('css', () => {
  const spriteData = gulp.src('src/sprites/*.png')
    .pipe(spritesmith({
      imgName: 'sprite.png',
      cssName: 'sprite.css',
      algorithm: 'alt-diagonal'
    }));

  const imgStream = spriteData.img
    .pipe(plumber())
    .pipe(buffer())
    .pipe(imagemin())
    .pipe(gulp.dest('dist'));

  const scss = gulp.src(['src/*.scss', 'node_modules/tiny-slider/src/tiny-slider.scss'])
    .pipe(plumber())
    .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))

  const cssStream = merge(spriteData.css, scss)
    .pipe(plumber())
    .pipe(concatCss('app.min.css', { rebaseUrls: false }))
    .pipe(minifyCSS())
    .pipe(gulp.dest('dist'));

  return merge(imgStream, cssStream);
});

gulp.task('default', () => {
  plumber = plumber.stop;
  ['pug', 'css', 'js', 'static'].forEach(x => gulp.start(x));
});

gulp.task('watch', () => {
  ['pug', 'css', 'js', 'static'].forEach(x => gulp.start(x));
  gulp.watch('src/*.js', ['js']);
  gulp.watch('src/*.pug', ['pug']);
  gulp.watch('src/*.scss', ['css']);
  gulp.watch('src/sprites/*', ['css']);
  gulp.watch('src/static/*', ['static']);
});
