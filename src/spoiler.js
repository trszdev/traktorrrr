import $ from 'jquery';

const addSpoilerBehaviour = ($spoilerContent, $spoilerBtn) => {
    $spoilerBtn.click(() => {
        $spoilerContent.toggleClass('hidden');
        $spoilerBtn.children('p').toggleClass('icon-top-arrow');
        $spoilerBtn.children('p').toggleClass('icon-bottom-arrow');
    });
};

addSpoilerBehaviour($('#spoiler1content'), $('#spoiler1'));
addSpoilerBehaviour($('#spoiler2content'), $('#spoiler2'));