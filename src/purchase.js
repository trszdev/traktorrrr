import $ from 'jquery';

const rerender = () => {
    const price = 25;
    const checked = [1, 2, 3]
        .map(x => $(`#char${x}`).prop('checked') ? 1 : 0)
        .reduce((acc, x) => acc + x);
    $('.purchase-price').text(`${25 + checked * 100}.000 P.`);
}

$('.purchase-checkbox').change(rerender);
$('.purchase-form').submit(() => false);
