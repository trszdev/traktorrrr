import $ from 'jquery';


const updateGuestBook = () => {
  $('.guest-book-btn.icon-pencil').unbind('click').click(function() {
    const $span = $(this).siblings('span');
    const newText = prompt(`Введите новое название для '${$span.text()}'`);
    if (newText !== null)
      $span.text(newText);
  });
  $('.guest-book-item > .icon-cancel').unbind('click').click(function() {
    $(this).parent().remove();
  });
}

const escapeHtml = text => $('<div/>').text(text).html();

const $guestBookList = $('.guest-book-list');
updateGuestBook();
$('.guest-book-input-group > .icon-ok').click(function () {
  const text = escapeHtml($(this).siblings('input').val());
  $guestBookList.append(`<li class='guest-book-item'>
    <span>${text}</span>
    <button class='guest-book-btn icon-pencil' />
    <button class='guest-book-btn icon-cancel' />
  </li>`);
  updateGuestBook();
});

$('.guest-book-input-group > .icon-cancel').click(function () {
  $(this).siblings('input').val('');
});