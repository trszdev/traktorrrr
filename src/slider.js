import { tns } from 'tiny-slider/src/tiny-slider.module';
import $ from 'jquery';

const slider = tns({
    container: '.slider-imgs',
    items: 1,
    controls: false,
    nav: false,
    autoplayButton: false,
    autoplayButtonOutput: false,
    autoplayTimeout: 15000,
    speed: 800,
    slideBy: 'page',
    autoplay: true
});

$('.slider-control.icon-left-arrow').click(() => slider.goTo('prev'));
$('.slider-control.icon-right-arrow').click(() => slider.goTo('next'));
$('.slider-img.hidden').toggleClass('hidden');
